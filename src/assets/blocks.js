const ipsum = `
  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
  Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
  Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
  Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
  Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
  Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
  Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
  Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
`

const blocks = {
  fullWidth: [
    {
      text: ipsum,
      image: 'bruno-van-der-kraan-1320028-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'erika-goodthing-1452523-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'julian-hochgesang-626029-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'matthias-tillen-1443893-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'daniil-vnoutchkov-1408981-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'fabrizio-conti-1444081-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'kamil-kalbarczyk-1220159-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'natalie-chaney-1400174-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'eberhard-grossgasteiger-1258904-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'jcob-nasyr-1472719-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'lucas-miguel-1329085-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'ruud-luijten-1551205-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'eberhard-grossgasteiger-1531174-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'johan-jensen-1107529-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'mar-bustos-1352933-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'visar-neziri-1468935-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'elie-khoury-1447906-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'julia-solonina-1331758-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'massimiliano-morosinotto-1551589-unsplash.jpg'
    },
    {
      text: ipsum,
      image: 'waranont-joe-1519895-unsplash.jpg'
    }
  ]
}
export {
  blocks
}
