import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/sample',
      name: 'sample',
      component: () => import(/* webpackChunkName: "sample" */ './views/Sample.vue')
    },
    {
      path: '/sample-lazy',
      name: 'sample-lazy',
      component: () => import(/* webpackChunkName: "sample-lazy" */ './views/SampleLazy.vue')
    },
    {
      path: '/sample-lazy-yall',
      name: 'sample-lazy-yall',
      component: () => import(/* webpackChunkName: "sample-lazy-yall" */ './views/SampleLazyYall.vue')
    },
    {
      path: '/picture-lazy',
      name: 'picture-lazy',
      component: () => import(/* webpackChunkName: "picture-lazy" */ './views/PictureLazy.vue')
    },
    {
      path: '/picture-lazy-yall',
      name: 'picture-lazy-yall',
      component: () => import(/* webpackChunkName: "picture-lazy-yall" */ './views/PictureLazyYall.vue')
    }
  ]
})
